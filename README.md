# Examples converting `only`/`except` to `rules`

Examples converting the `only`/`except` syntax to `rules` syntax for common scenarios.

- [Rules and merge request pipelines](#rules-and-merge-request-pipelines)
- [Run only on master branch](#run-only-on-master-branch)
- [Run on all branches that are not master](#run-on-all-branches-that-are-not-master)
- [Run only on branches or tags](#run-only-on-branches-or-tags)
- [Run only on tags](#run-only-on-tags)
- [Run only on schedule pipelines](#run-only-on-schedule-pipelines)
- [Allow failure unless on schedule pipeline](#allow-failure-unless-on-schedule-pipeline)

## Rules and merge request pipelines

The default behavior with `rules` for merge request pipelines is opposite that of `only` - a merge request job (and pipeline if required) is also created if a rule matches (versus `only` where `merge_request` has to be specifically called out).  This can be disabled per job with: 

```yaml
   rules:
     - if: $CI_MERGE_REQUEST_ID
       when: never
```

To globally disable, use the following.  Note the use of `when: always` (versus, for example, `when: on_success`) for the default case since [only `always` and `never` are allowed for `when` under `workflow`](https://docs.gitlab.com/ee/ci/yaml/#workflowrules).

```yaml
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - when: always
```

Note: Merge request pipeline are run in the context of the merge request and therefore have no branch or tag, i.e. neither `$CI_COMMIT_BRANCH` nor `$CI_COMMIT_TAG` are populated, so any logic relying only on those variables having values will not create a merge request job.

## Run only on master branch

**from**

```yaml
  only:
    - master
```

**to**

```yaml
   rules:
     - if: $CI_COMMIT_BRANCH == "master"
```

## Run on all branches that are not master

**from**

```yaml
  only:
    - branches
  except:
    - master
```

**to**

```yaml
   rules:
     # Without the initial check for $CI_COMMIT_BRANCH, this will also run on tag pipelines
     - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH != "master"
```

## Run only on branches or tags

**from**

```yaml
  only:
    - branches
    - tags
```

**to**

```yaml
  rules:
    - if: $CI_COMMIT_BRANCH || $CI_COMMIT_TAG
```

## Run only on tags

**from**

```yaml
  only:
    - tags
```

**to**

```yaml
  rules:
    - if: $CI_COMMIT_TAG
```

## Run only on schedule pipelines

**from**

```yaml
  only:
    - schedules
```

**to**

```yaml
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
```

## Allow failure unless on schedule pipeline

**from**

```yaml
job1:
  except:
    refs:
      - schedules
  allow_failure: true

job1_fail:
  extends: job1
  only:
    refs:
      - schedules
  # Override except otherwise it will inherit except schedules and never run
  except:
    refs: []
  allow_failure: false
```

**to**

```yaml
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      allow_failure: false
    - allow_failure: true
```

Note: This example will also create a job in a merge request pipeline (and cause a merge request pipeline to be created if it was not otherwise). See [above](#rules-and-merge-request-pipelines) for details.
